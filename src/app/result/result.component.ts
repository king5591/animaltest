import { ActivatedRoute, Router } from '@angular/router';
import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'],
})
export class ResultComponent implements OnInit {
  name;
  animals;
  url = 'https://morning-ocean-20578.herokuapp.com/result';

  constructor(
    private netService: NetService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe((param) => {
      this.name = param.get('name');
      console.log(this.name);
      if (this.name) {
        this.netService
          .getData(this.url + '/' + this.name)
          .subscribe((resp) => {
            this.animals = resp;
            console.log(this.animals);
          });
      }
    });
  }
}
