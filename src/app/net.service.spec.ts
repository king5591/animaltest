/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NetService } from './net.service';

describe('Service: Net', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NetService]
    });
  });

  it('should ...', inject([NetService], (service: NetService) => {
    expect(service).toBeTruthy();
  }));
});
