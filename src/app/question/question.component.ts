import { Router, ActivatedRoute } from '@angular/router';
import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css'],
})
export class QuestionComponent implements OnInit {
  animals;
  url = 'https://morning-ocean-20578.herokuapp.com/animals';
  name;
  index = 0;
  answer = '';
  option;
  show = false;

  constructor(
    private netService: NetService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe((param) => {
      this.name = param.get('name');
    });
    this.netService.getData(this.url).subscribe((resp) => {
      this.animals = resp;
      this.index = 0;
      this.makeStructure();
    });
  }
  makeStructure() {
    if (this.animals) {
      this.option = {
        options: this.animals[this.index].option,
        answer: this.answer ? this.answer : '',
      };
    }
  }

  optChange() {
    if (this.option.answer) {
      this.answer = this.option.answer;

      setTimeout(() => {
        if (this.answer == this.animals[this.index].name) {
          this.animals[this.index].answer = this.answer;
          if (this.index === 4) {
            this.show = true;
          }
          if (this.index < 4) {
            this.index++;
            this.answer = '';
          }
          this.makeStructure();
        } else {
          alert(
            'You have selected wrong choice. Please select correct choice!!'
          );
          this.animals[this.index].answer = '';
        }
      }, 2000);
    }
  }
  submit() {
    let obj = { name: this.name, animals: this.animals };
    this.netService.postData(this.url, obj).subscribe((resp) => {

      this.router.navigate(['/result', this.name], { relativeTo: this.route });
    });
  }
}
