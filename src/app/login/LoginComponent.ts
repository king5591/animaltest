import { NetService } from './../net.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  url = 'https://morning-ocean-20578.herokuapp.com/login';

  constructor(
    private netService: NetService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.login = new FormGroup({
      name: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit() {}
  get name() {
    return this.login.get('name');
  }
  onSubmit() {
    let name1 = this.login.get('name').value;
    this.netService.postData(this.url, { name: name1 }).subscribe(
      (resp) => {
        this.router.navigate(['/question'], { queryParams: { name: name1 } });
      },
      (err) => {
        alert('Invalid credential. Please give a unique and valid one!!');
      }
    );
  }
}
