import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-option',
  templateUrl: './option.component.html',
  styleUrls: ['./option.component.css'],
})
export class OptionComponent implements OnInit {
  @Input() option;
  @Output() selOption = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  emitChange() {
    this.selOption.emit();
  }
}
